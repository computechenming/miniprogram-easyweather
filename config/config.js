const config = {
  name: '轻松天气',
  version: '1.3.7',
  versionDate: '2020.11.05',
  versionInfo: '\n- 问题修复',
  request: {
    host: 'https://ali-weather.showapi.com',
    header: { 'Authorization': 'APPCODE ' + '将你的 AppCode 填到这里' },
  },
}

export default config;
